package org.hse.template.converter

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonNode
import org.hse.template.client.rest.model.Weather
import org.springframework.stereotype.Component
import java.io.IOException
import java.time.LocalDateTime
import java.time.ZoneOffset

@Component
class WeatherDeserializer : JsonDeserializer<Weather?>() {
    @Throws(IOException::class)
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): Weather {
        val node: JsonNode = p.codec.readTree(p)
        val weather = Weather()
        weather.mainWeather = node["weather"][0]["main"].asText()
        weather.weatherDescription = node["weather"][0]["description"].asText()
        weather.temperature = node["main"]["temp"].asDouble()
        weather.pressure = node["main"]["pressure"].asInt()
        weather.humidity = node["main"]["humidity"].asInt()
        val timestamp = node["dt"].asLong()
        weather.dateTime = LocalDateTime.ofEpochSecond(timestamp, 0, ZoneOffset.UTC)
        weather.windSpeed = node["wind"]["speed"].asDouble()
        return weather
    }
}