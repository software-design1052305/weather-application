package org.hse.template.converter

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import org.hse.template.client.rest.model.Weather
import org.hse.template.client.rest.model.WeatherPredictJSONDeserialized
import java.io.IOException


class WeatherPredictDeserializer : JsonDeserializer<WeatherPredictJSONDeserialized?>() {
    @Throws(IOException::class)
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): WeatherPredictJSONDeserialized {
        val node: JsonNode = p.codec.readTree(p)
        val weatherPredictJSONDeserialized = WeatherPredictJSONDeserialized()
        val mapper = p.codec as ObjectMapper
        weatherPredictJSONDeserialized.weatherList = node["list"].map { mapper.treeToValue(it, Weather::class.java) }
        return weatherPredictJSONDeserialized
    }
}