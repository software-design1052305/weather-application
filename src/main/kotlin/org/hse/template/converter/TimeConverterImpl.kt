package org.hse.template.converter

import org.springframework.stereotype.Component

@Component
class TimeConverterImpl : TimeConverter {
    override fun convertFromHoursToMilliseconds(hours: Double): Long = (hours * 60 * 60 * 1000).toLong()
}