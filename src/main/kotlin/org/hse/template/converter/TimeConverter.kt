package org.hse.template.converter

import org.springframework.stereotype.Component


@Component
interface TimeConverter {
    fun convertFromHoursToMilliseconds(hours: Double): Long
}