package org.hse.template.controller

import io.swagger.v3.oas.annotations.media.Schema
import org.hse.template.api.WeatherApi
import org.hse.template.client.rest.model.Alert
import org.hse.template.client.rest.model.Weather
import org.hse.template.service.WeatherService
import org.hse.template.service.WeatherUpdateService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class WeatherController(
    private val weatherService: WeatherService,
    private val weatherUpdateService: WeatherUpdateService
) : WeatherApi {

    @GetMapping("/weather/{city}")
    override fun cityCurrentWeather(
        @Schema(
            description = "city Name",
            example = "London",
            type = "string"
        ) @PathVariable("city") city: String
    ): Weather {
        return weatherService.cityCurrentWeather(city)
    }

    @GetMapping("/forecast/{city}")
    override fun cityFiveDaysForecast(@PathVariable("city") city: String): List<Weather> {
        return weatherService.cityFiveDaysForecast(city)
    }

    @GetMapping("/alerts/{city}")
    override fun alerts(@PathVariable("city") city: String): List<Alert> {
        return weatherService.cityAlerts(city)
    }

    @PostMapping("/update")
    override fun update() {
        weatherUpdateService.update()
    }
    /*@GetMapping("/historical/{city}")
    override fun historical(@PathVariable("city") cityName: String) = weatherClient.random(amount)*/


}