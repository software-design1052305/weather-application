package org.hse.template.api

import io.swagger.v3.oas.annotations.media.Schema
import org.hse.template.client.rest.model.Alert
import org.hse.template.client.rest.model.Weather

interface WeatherApi {

    fun cityCurrentWeather(
        @Schema(
            description = "Название города",
            type = "string",
            example = "London"
        )
        city: String
    ): Weather

    fun cityFiveDaysForecast(
        @Schema(
            description = "Название города",
            type = "string",
            example = "London"
        )
        city: String
    ): List<Weather>

    fun alerts(
        @Schema(
            description = "Название города",
            type = "string",
            example = "London"
        )
        city: String
    ): List<Alert>

    fun update(
    )
}