package org.hse.template.configuration

import org.springframework.boot.context.properties.ConfigurationProperties


@ConfigurationProperties(prefix = "weather")
data class OpenWeatherApiProperties(
    val connection: ConnectionProperties,
    val time: TimeProperties,
    val units: String,
    val language: String,
)

@ConfigurationProperties(prefix = "weather.connection")
data class ConnectionProperties(
    val apiKey: String
)

@ConfigurationProperties(prefix = "weather.time")
data class TimeProperties(
    val deltaTime: Long,
    val forecastTime: Double,
)