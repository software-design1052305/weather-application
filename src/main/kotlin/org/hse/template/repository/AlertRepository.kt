package org.hse.template.repository

import org.hse.template.client.rest.model.Alert
import org.hse.template.client.rest.model.City
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
interface AlertRepository : JpaRepository<Alert, Long> {
    fun findByCityAndEndDateTimeAfter(city: City, endDateTime: LocalDateTime): List<Alert>
}