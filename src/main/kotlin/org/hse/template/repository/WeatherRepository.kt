package org.hse.template.repository

import org.hse.template.client.rest.model.Weather
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface WeatherRepository : JpaRepository<Weather, Long>