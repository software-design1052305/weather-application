package org.hse.template.repository

import org.hse.template.client.rest.model.City
import org.hse.template.client.rest.model.CurrentWeather
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface CurrentWeatherRepository : JpaRepository<CurrentWeather, Long> {
    fun findByCity(city: City?): Optional<CurrentWeather>
}