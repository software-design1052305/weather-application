package org.hse.template

import org.hse.template.configuration.ConnectionProperties
import org.hse.template.configuration.OpenWeatherApiProperties
import org.hse.template.configuration.TimeProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@EnableConfigurationProperties(OpenWeatherApiProperties::class, ConnectionProperties::class, TimeProperties::class)
@SpringBootApplication
class Application

fun main(args: Array<String>) {
    runApplication<Application>(*args)
}
