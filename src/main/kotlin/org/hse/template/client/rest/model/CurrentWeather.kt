package org.hse.template.client.rest.model

import io.swagger.v3.oas.annotations.media.Schema
import jakarta.persistence.*
import lombok.AccessLevel
import lombok.Data
import lombok.NoArgsConstructor


@Entity
@Schema(description = "Информация о погоде")
@NoArgsConstructor(access = AccessLevel.PUBLIC, force = true)
@Data
@Table(name = "current_weather")
class CurrentWeather(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long,
    @ManyToOne
    @JoinColumn(name = "city_id")
    var city: City? = null,
    @OneToOne
    @JoinColumn(name = "weather_id")
    var weather: Weather? = null
) {
    constructor() : this(0, null, null)
}
