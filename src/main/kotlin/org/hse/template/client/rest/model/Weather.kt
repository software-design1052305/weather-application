package org.hse.template.client.rest.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import io.swagger.v3.oas.annotations.media.Schema
import jakarta.persistence.*
import lombok.AccessLevel
import lombok.Data
import lombok.NoArgsConstructor
import org.hse.template.converter.WeatherDeserializer
import java.time.LocalDateTime

@Entity
@Schema(description = "Информация о погоде")
@NoArgsConstructor(access = AccessLevel.PUBLIC, force = true)
@Data
@JsonDeserialize(using = WeatherDeserializer::class)
@Table(name = "weather")
class Weather(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    var id: Long,
    var mainWeather: String,
    var weatherDescription: String,
    var temperature: Double,
    @Schema(description = "Атмосферное давление в гПа")
    var pressure: Int,
    @Schema(description = "Влажность в %")
    var humidity: Int,
    @Schema(description = "Время GMT")
    var dateTime: LocalDateTime,
    @Schema(description = "Скорость ветра в м/с")
    var windSpeed: Double
) {
    constructor() : this(0, "", "", 0.0, 0, 0, LocalDateTime.now(), 0.0)
}