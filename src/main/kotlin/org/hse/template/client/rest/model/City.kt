package org.hse.template.client.rest.model

import io.swagger.v3.oas.annotations.media.Schema
import jakarta.persistence.*
import lombok.AccessLevel
import lombok.Data
import lombok.NoArgsConstructor

@Entity
@Schema(description = "Информация о городе")
@Data
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Table(name = "city")
class City(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private val id: Long,
    @Schema(description = "Название города")
    var name: String,
    var lat: Double,
    var lon: Double,
    var country: String,
)
