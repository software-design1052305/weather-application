package org.hse.template.client.rest.model

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import org.hse.template.converter.WeatherPredictDeserializer

@JsonDeserialize(using = WeatherPredictDeserializer::class)
data class WeatherPredictJSONDeserialized(
    var weatherList: List<Weather>
) {
    constructor() : this(listOf())
}