package org.hse.template.client.rest.model

import io.swagger.v3.oas.annotations.media.Schema
import jakarta.persistence.*
import lombok.AccessLevel
import lombok.Data
import lombok.NoArgsConstructor
import java.time.LocalDateTime

@Entity
@Schema(description = "Информация о погоде")
@NoArgsConstructor(access = AccessLevel.PUBLIC, force = true)
@Data
@Table(name = "weather_predict")
class WeatherPredict(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long,
    @ManyToOne
    @JoinColumn(name = "city_id")
    var city: City? = null,
    @OneToMany
    @JoinColumn(name = "weather_id")
    var weatherList: List<Weather>,
    var dateTime: LocalDateTime? = null
) {
    constructor() : this(0, null, listOf(), null)
}