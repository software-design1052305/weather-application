package org.hse.template.client.rest.api

import org.hse.template.client.rest.model.AlertsJSONDeserialized
import org.hse.template.client.rest.model.City
import org.hse.template.client.rest.model.Weather
import org.hse.template.client.rest.model.WeatherPredictJSONDeserialized
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

@Component
@FeignClient(name = "open-weather")
interface WeatherClient {

    @GetMapping("/geo/1.0/direct")
    fun city(
        @RequestParam("q") cityName: String,
        @RequestParam("appid") apiKey: String,
        @RequestParam("limit") limit: Int = 1,
    ): List<City>

    @GetMapping("/data/2.5/weather")
    fun currentWeather(
        @RequestParam("lat") lat: Double, @RequestParam("lon") lon: Double, @RequestParam("appid") apiKey: String,
        @RequestParam("lang") language: String, @RequestParam("units") units: String
    ): Weather

    @GetMapping("/data/2.5/forecast")
    fun weatherFiveDaysForecast(
        @RequestParam("lat") lat: Double, @RequestParam("lon") lon: Double, @RequestParam("appid") apiKey: String,
        @RequestParam("lang") language: String, @RequestParam("units") units: String
    ): WeatherPredictJSONDeserialized

    @GetMapping("/data/2.5/onecall")
    fun alerts(
        @RequestParam("lat") lat: Double, @RequestParam("lon") lon: Double, @RequestParam("appid") apiKey: String,
        @RequestParam("exclude") exclude: String = "current,minutely,hourly,daily"
    ): AlertsJSONDeserialized

    /*@GetMapping("/historical/{city}")
    fun historical(@PathVariable("city") cityName: String)*/
}
