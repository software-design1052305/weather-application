package org.hse.template.client.rest.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import io.swagger.v3.oas.annotations.media.Schema
import jakarta.persistence.*
import lombok.AccessLevel
import lombok.Data
import lombok.NoArgsConstructor
import org.hse.template.converter.DateTimeDeserializer
import java.time.LocalDateTime

@Entity
@Schema(description = "Информация о погодных предупреждениях")
@NoArgsConstructor(access = AccessLevel.PUBLIC, force = true)
@Data
@Table(name = "alert")
class Alert(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    var id: Long,
    @JsonProperty("sender_name")
    var senderName: String,
    var event: String,
    @Schema(description = "Время GMT")
    @JsonProperty("start")
    @JsonDeserialize(using = DateTimeDeserializer::class)
    var startDateTime: LocalDateTime,
    @Schema(description = "Время GMT")
    @JsonProperty("end")
    @JsonDeserialize(using = DateTimeDeserializer::class)
    var endDateTime: LocalDateTime,
    var description: String,
    @ManyToOne
    @JoinColumn(name = "city_id")
    var city: City? = null
) {
    constructor() : this(0, "", "", LocalDateTime.now(), LocalDateTime.now(), "", null)
}