package org.hse.template.service


import org.springframework.stereotype.Service

@Service
interface WeatherUpdateService {
    fun update()
}
