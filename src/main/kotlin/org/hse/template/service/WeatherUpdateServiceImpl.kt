package org.hse.template.service

import org.hse.template.repository.CityRepository
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service


@Service
class WeatherUpdateServiceImpl(
    private val cityRepository: CityRepository,
    private val weatherService: WeatherService
) : WeatherUpdateService {
    override fun update() {
        updateCurrentWeather()
        updateForecast()
        updateAlert()
    }

    @Scheduled(fixedRateString = "\${weather.update-time.current-weather-time}")
    fun updateCurrentWeather() {
        cityRepository.findAll().forEach {
            weatherService.getCurrentWeatherByCity(it, true)
        }
    }

    @Scheduled(fixedRateString = "\${weather.update-time.forecast-time}")
    fun updateForecast() {
        cityRepository.findAll().forEach {
            weatherService.getFiveDayForecastByCity(it, true)
        }
    }

    @Scheduled(fixedRateString = "\${weather.update-time.alert-time}")
    fun updateAlert() {
        cityRepository.findAll().forEach {
            weatherService.getAlertsByCity(it, true)
        }
    }

}